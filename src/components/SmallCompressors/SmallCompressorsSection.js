import React from 'react'
import SmallCompressorsItem from './SmallCompressorsItem'
import styled from 'styled-components'

const SectionTitle = styled.h2`
  color: #0071b9;
`

const Row = styled.div`
  display: flex;
  gap: 2.5em;
  margin: 0 0 1.5rem 0;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`

const SmallCompressorsSection = () => {
  return (
    <>
    <SectionTitle>Vorteile im Detail</SectionTitle>
      <Row>
      <SmallCompressorsItem
        img="https://www.boge.com/sites/row/files/styles/branche_460x238/public/waermerueckgewinnung-frequenzumrichter_boge-kompressoren.jpg"
        title="OPTIONALE AUSSTATTUNGSMÖGLICHKEITEN"
        description="Einen Minikompressor der C-Baureihe bis 7,5 kW und auch die größeren Kompressoren können Sie nach Bedarf mit einem Kältetrockner ausstatten, um ohne zusätzlichen Platzbedarf trockene Druckluft zu erzeugen. Außerdem lassen sich die Schraubenkompressoren bis 22kW auch mit unserem System zur Wärmerückgewinnung (DUOTHERM) ausrüsten. Damit werden bis zu 72 % der aufgenommenen Energie abgeführt, die zur effizienten Brauchwassererwärmung oder Raumheizung genutzt werden kann. Das trägt zu einer schnellen Amortisation der gesamten Druckluftanlage bei. Daneben macht sich besonders bei schwankendem Druckluftbedarf eine integrierte Frequenzregelung bezahlt, welche die Liefermenge individuell auf den benötigten Bedarf anpasst. Weitere Optionen wie IE 4 Motoren, frequenzgeregelte Lüfter und Behälter bis 22 kW sorgen für eine anschlussfertige Komplettlösung zugeschnitten auf die jeweiligen Bedürfnisse."
      />
      <SmallCompressorsItem
        img="https://www.boge.com/sites/row/files/styles/branche_460x238/public/klein-kompakt-platzsparend_boge-industriekompressoren.jpg"
        title="KOMPAKT UND ZUVERLÄSSIG"
        description="Wir haben alle notwendigen Komponenten komplett integriert und die Wartungs- und Verschleißteile so angeordnet, dass sie leicht zugänglich sind. Damit ist ein Kompressor der C-Baureihe von BOGE verhältnismäßig klein und platzsparend. Dennoch bietet er als Industriekompressor höchste Betriebssicherheit und maximalen Komfort.

        Durch die Integration der Bauteile in eine kompakte Einheit entfallen Verbindungsleitungen und Rohre. Damit werden Druckverluste minimiert und Leckagen verhindert. Darüber hinaus verfügt unsere spezielle BOGE Verdichterstufe über einen besonders niedrigen Leistungsbedarf. Das heißt, unsere Industriekompressoren arbeiten maximal zuverlässig und effizient. "
      />
      </Row>
      <Row>
      <SmallCompressorsItem
        img="https://www.boge.com/sites/row/files/styles/branche_460x238/public/vibrationsarm-ohne-schalldaemmung_boge-minikompressoren.jpg"
        title="LEISE UND VIBRATIONSARM"
        description="Alle Modelle der neusten Generation profitieren von der serienmäßigen Schalldämmhaube, die Betriebsgeräusche und Vibrationen erfolgreich minimiert. Durch die optionale Superschalldämmung lässt sich der Schallpegel noch deutlicher reduzieren – beste Bedingungen für eine flexible Aufstellung."
      />
      <SmallCompressorsItem
        img="https://www.boge.com/sites/row/files/styles/branche_460x238/public/boge-service_montage-und-lieferung2.jpg"
        title="MONTAGE & LIEFERUNG"
        description="Wir montieren alle Einzelelemente und liefern die Kompressoren als anschlussfertige Kompakteinheit an Sie aus, damit Sie in jeder Anwendungssituation zuverlässig und wirtschaftlich arbeiten können."
      />
      </Row>
    </>
  )
}

export default SmallCompressorsSection
