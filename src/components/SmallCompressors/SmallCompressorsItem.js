import React from 'react'
import styled from 'styled-components'

const Card = styled.div`
  background-color: #fff;
  flex: 1;
`
const Img = styled.img`
  width: 100%;
  height: 188px;

  @media (max-width: 768px) {
    object-fit: scale-down;
  }
`
const Content = styled.div`
  padding: 15px;
  line-height: 1.4em;
`

const SmallCompressorsItem = (props) => {
  return (
    <Card>
      <Img src={props.img} alt="CompressorImg" ></Img>
      <Content>
        <h4>{props.title}</h4>
        <p>
          {props.description}
       </p>
      </Content>
    </Card>
  )
}

export default SmallCompressorsItem
