import React from 'react'
import styled from 'styled-components'

//Wrapper only for development to simulate real page width
const Wrapper = styled.main`
  max-width: 940px;
  margin: auto;
  padding: 15px;

  @media (max-width: 768px) {
    max-width: 85%;
  }
`

const MainWrapper = ({children}) => {
  return (
    <Wrapper>{children}</Wrapper>
  )
}

export default MainWrapper
