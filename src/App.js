import './App.css';
import MainWrapper from './MainWrapper';
import SmallCompressorsSection from './components/SmallCompressors/SmallCompressorsSection';

function App() {
  return (
    <div className="App">
      <MainWrapper>
        <SmallCompressorsSection />
      </MainWrapper>
    </div>
  );
}

export default App;
